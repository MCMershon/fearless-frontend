function createCard(name, description, pictureUrl, dateSpread, locationName) {
  return `
    <div class="card" style="margin-bottom: 25px; box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.3);">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
        <p class="card-text">${description}</p>
      </div>
    <div class="card-footer" style="padding-bottom: 5.8px; padding-top: 5px;">
      <p class="card-footer-text" style="margin-bottom: 0px;">${dateSpread}</p>
    </div>
    </div>
  `;
}

function displayError(errorMessage) {
  return `
    <div class="alert alert-danger" role="alert">
      ${errorMessage}
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {

        const errorMessage = "ERROR: Failed to load conferences!";
        const container = document.querySelector(`.container`);
        container.innerHTML += displayError(errorMessage);

        throw new Error("Failed to fetch conferences with given URL");

      } else {

        const data = await response.json();
        let colNum = 1

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            if (colNum === 4) {
              colNum = 1;
            }

            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const startDate = new Date(details.conference.starts);
            const startMonth = startDate.getMonth();
            const startDay = startDate.getDate() + 1;
            const startYear = startDate.getFullYear();

            const endDate = new Date(details.conference.ends);
            const endMonth = endDate.getMonth();
            const endDay = endDate.getDate() + 1;
            const endYear = endDate.getFullYear();

            const dateSpread = `${startMonth}/${startDay}/${startYear} - ${endMonth}/${endDay}/${endYear}`

            const locationName = details.conference.location.name;

            const html = createCard(name, description, pictureUrl, dateSpread, locationName);
            const column = document.querySelector(`.col${colNum}`);
            column.innerHTML += html;
            colNum++;
          }
        }
      }
    } catch (e) {
      console.log("Error: ", e);
    }
});
